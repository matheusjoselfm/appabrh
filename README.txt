(Ionic CLI)           : 3.20.0       -       npm install -g ionic@3.20.0
cordova (Cordova CLI) : 8.0.0
Cordova Platforms     : android 7.0.0
Ionic Framework       : ionic-angular 3.9.2
Android SDK Tools     : 26.1.1
Node                  : v8.9.4
npm                   : 5.6.0
