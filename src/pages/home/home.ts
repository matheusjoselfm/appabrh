import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { ComponentHelperService } from '../../providers/component-helper/component-helper.service';

import { FCM } from '@ionic-native/fcm';
import { ParameterHelperService } from '../../providers/parameter-helper/parameter-helper.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public componentHelperService: ComponentHelperService, public fcm: FCM, public parameterHelperService: ParameterHelperService) {
    this.menuCtrl.swipeEnable(true);
  }

  showToast() {
    this.componentHelperService.presentToast("teste 1001");
  }

  showLoading() {
    let loading = this.componentHelperService.presentLoading();
    setTimeout(function(){ 
      loading.dismiss();
    }, 3000);
  }

  showAlert() {
    this.componentHelperService.presentAlert("Teste Title", "Teste Subt");
  }

  registerFcmToken() {//PEGA TOKEN PARA USAR NO PUSH NOTIFICATION
    this.fcm.getToken().then(token => {
      console.log(token);
    });

    this.fcm.onTokenRefresh().subscribe(token => {
      console.log(token);
    });
  }

}
