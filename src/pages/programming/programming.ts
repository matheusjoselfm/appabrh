import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MapsPage } from '../maps/maps';

@Component({
  selector: 'page-programming',
  templateUrl: 'programming.html',
})
export class ProgrammingPage {

  public events: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.events = [{
      id: 1,
      name: "HR Conference",
      time: "10h",
      thumb: "assets/imgs/event_1.jpg"
    },
    {
      id: 2,
      name: "Como Formar Mentes Brilhantes",
      time: "14h",
      thumb: "assets/imgs/event_2.jpg"
    },
    {
      id: 3,
      name: "HSM Leadership Summit",
      time: "16h",
      thumb: "assets/imgs/event_3.jpg"
    },
    {
      id: 4,
      name: "Summit Brasil",
      time: "18h",
      thumb: "assets/imgs/event_4.jpg"
    }];
  }

  ionViewDidLoad() {
    
  }

  openGoogleMaps() {
    this.navCtrl.push(MapsPage);
  }

}
