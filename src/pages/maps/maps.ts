import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GoogleMaps, GoogleMap, MyLocation, LocationService, GoogleMapOptions, Marker } from '@ionic-native/google-maps';

@Component({
	selector: 'page-maps',
	templateUrl: 'maps.html',
})
export class MapsPage {

	map: GoogleMap;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

	ionViewDidLoad() {
		this.loadMap();
	}

	loadMap() {
		LocationService.getMyLocation().then((myLocation: MyLocation) => {
			let options: GoogleMapOptions = {
				camera: {
					target: myLocation.latLng,
					zoom: 16
				}
			};

			// Create a map after the view is ready and the native platform is ready.
			this.map = GoogleMaps.create('map_canvas', options);

			let marker: Marker = this.map.addMarkerSync({
				title: 'HR Conference',
				icon: 'blue',
				animation: 'DROP',
				position: myLocation.latLng,
			});
	
			marker.showInfoWindow();
		});
		
	}

}
