import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class ParameterHelperService {

  constructor(public storage: Storage) { }

  /* --------- Parameter get & set ---------- */
	setParameter(key, value: any) {
		return this.storage.set(`${key}`, value);
	}

	getParameter(key): Promise<string> {
		return this.storage.get(`${key}`);
	}
  /* ---------------------------------------- */
  
}
