import { Injectable } from '@angular/core';

import { ToastController, LoadingController, AlertController } from 'ionic-angular';

@Injectable()
export class ComponentHelperService {

  constructor(public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {}

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  presentLoading(): any {
    let loader = this.loadingCtrl.create();
    loader.present();
    return loader;
  }

  presentAlert(title, subTitle, ) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }

}
