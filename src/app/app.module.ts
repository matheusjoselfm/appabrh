import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ProgrammingPage } from '../pages/programming/programming';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NewsPage } from '../pages/news/news';
import { LoginPage } from '../pages/login/login';
import { MapsPage } from '../pages/maps/maps';

import { GoogleMaps } from "@ionic-native/google-maps";
import { EventPage } from '../pages/event/event';
import { RegisterPage } from '../pages/register/register';
import { ForgotPassPage } from '../pages/forgot-pass/forgot-pass';
import { AboutEventPage } from '../pages/about-event/about-event';
import { SponsorsPage } from '../pages/sponsors/sponsors';

import { ComponentHelperService } from '../providers/component-helper/component-helper.service';
import { FCM } from '@ionic-native/fcm';
import { ParameterHelperService } from '../providers/parameter-helper/parameter-helper.service';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    RegisterPage,
    ForgotPassPage,
    HomePage,
    MapsPage,
    ProgrammingPage,
    EventPage,
    NewsPage,
    AboutEventPage,
    SponsorsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, { scrollPadding: false, scrollAssist: false }),
    IonicStorageModule.forRoot({ name: '__dynamicbox', storeName: 'parameters', driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage'] }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RegisterPage,
    ForgotPassPage,
    HomePage,
    MapsPage,
    ProgrammingPage,
    EventPage,
    NewsPage,
    AboutEventPage,
    SponsorsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ComponentHelperService,
    FCM,
    ParameterHelperService
  ]
})
export class AppModule {}
