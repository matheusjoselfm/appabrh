import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ProgrammingPage } from '../pages/programming/programming';
import { NewsPage } from '../pages/news/news';
import { LoginPage } from '../pages/login/login';
import { SponsorsPage } from '../pages/sponsors/sponsors';
import { AboutEventPage } from '../pages/about-event/about-event';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'md-home' },
      { title: 'Programação', component: ProgrammingPage, icon: 'md-calendar'},
      { title: 'Notícias', component: NewsPage, icon: 'md-paper'},
      { title: 'Sobre o Evento', component: AboutEventPage, icon: 'md-information'},
      { title: 'Patrocinadores', component: SponsorsPage, icon: 'md-people'}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
